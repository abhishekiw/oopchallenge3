﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge3
{
    public class LIS
    {
        int n, i, count = 0;
        int[] array = new int[100];

        //get user input
        public void getinput()
        {

            try
            {

                Console.WriteLine("Enter total number of numbers in array: ");
                n = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the numbers in array: ");
                for (i = 0; i < n; i++)
                {
                    array[i] = Convert.ToInt32(Console.ReadLine());
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter the number!");
                getinput();
            }

        }
        //calculate the LIS
        public void calculate()
        {
            try
            {
                int[] array2 = new int[100];
                for (i = 0; i < n; i++)
                {
                    array2[i] = array[i];
                }

                Console.WriteLine(Environment.NewLine + "The longest increasing subsequence is: ");
                Console.WriteLine(Environment.NewLine + array2[0]);
                for (i = 0; i < n; i++)
                {
                    if (array2[i] < array2[i + 1])
                    {
                        array[i + 1] = array[i + 1];
                        count++;
                        Console.WriteLine(Environment.NewLine + array[i + 1]);
                    }

                }
            }
            catch(Exception)
            {
                Console.WriteLine("Please enter the number!");
                calculate();
            }
            Console.WriteLine(Environment.NewLine + "The total Count of LIS is " + (count + 1));
            Console.Write("The process completed.");
        }

    }
    
}